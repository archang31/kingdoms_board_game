
import view.GUIView;
import view.TextView;

import view.GameObserver;
import model.Board;
import controller.ControllerInterface;
import controller.GameController;

public class GameTester {

	final static int TILE_WIDTH = 125;
	final static int NUM_ROWS = 5;
	final static int NUM_COLUMNS = 6;
	
	public static void main(String[] args) {
		
		Board model = new Board(TILE_WIDTH,NUM_ROWS,NUM_COLUMNS);
		
		
		GameObserver view1 = new GUIView(model);
		GameObserver view2 = new TextView(model);
		
	
		
		ControllerInterface controller1 = new GameController(model, TILE_WIDTH, NUM_ROWS, NUM_COLUMNS);
		

	}

}
