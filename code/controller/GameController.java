package controller;

import java.awt.Color;
import java.util.ArrayList;

import model.BenefitSelf;
import model.Biggest;
import model.Board;
import model.DumbStrat;
import model.Human;
import model.HurtsOthers;
import model.Move_Result;
import model.Player;
import model.Slot;
import model.Strategy;
import view.GUIView;
import view.GameObserver;
import view.TextView;

public class GameController implements ControllerInterface {


	Board model;
	GameObserver view;
	TextView tex;
	Player Blue;
	Player Green;
	Player Red;
	Player Yellow;           // there will alawys be 4 players no squabbles
	Color c ;
	Board forNextGame;
	int tileWidth;
	int x;
	int y;
	
	State preGame;
	State gameOn;
	State gameOver;
	
	State state = preGame;
	
	
	public GameController(Board model,int tileWidth, int x, int y) {
		
		this.model = model;
		preGame= new PreGame(this);
		gameOn = new GameOn(this);
		gameOver = new GameOver(this);
		
		state=preGame;
		
		view=(GameObserver) model.getObservers().get(0);    //hackey way to get easy acces to GUI view, it will always be first in the observers array.. a legit assumption
		tex=(TextView) model.getObservers().get(1);      //more hackery jsut for testing, will go awaay, i promis
		this.tileWidth=tileWidth;
		this.x =x;
		this.y =y;	
		view.registerController(this);
		view.createDisplay(tileWidth);
		
		
		
//		put avialible strategies in an array list;  
		//if you make a new strategy, let the game know aobut ti by jsut adding 1 line here
	
		model.addStrategy(new BenefitSelf());
		model.addStrategy(new HurtsOthers());
		model.addStrategy(new Biggest());
		model.addStrategy(new DumbStrat());
		model.addStrategy(new Human());
	
	
		Strategy a = model.getAvalibleStrategies().get(3);
		setRed(new Player(Color.RED,a,model.getTile(),"Red Player"));
		setBlue(new Player(Color.BLUE,a,model.getTile(),"Blue Player"));
		setGreen(new Player(Color.GREEN,a,model.getTile(),"Green Player"));
		setYellow(new Player(Color.YELLOW,a,model.getTile(),"Yellow Player"));
	
		//		 Initializing the turn order
		model.addPlayer(Red);                 
		model.addPlayer(Blue);
		model.addPlayer(Green);
		model.addPlayer(Yellow);
	
		determineStart();     //randomizes who starts

		// State pattern type stuff yo yo
		doIt();
	

		
	}
	
	public ReturnStuff newGame() {
		
		setState(getPreGame());
		doIt();
		//showThisMessage( "Starts New Game");
		return ReturnStuff.good;
	}
	
	private void showThisMessage(String s)
	{
		ArrayList temp =model.getObservers();
		
		if(!temp.isEmpty())
		{
			for(int i=0; i< temp.size();i++)
			{
				((GameObserver) temp.get(i)).showThisMessage(s);
			}
		}
		
	}
	
	public TextView giveTex()
	{
		return tex;
	}
	
	public String showStrategy(Player play) {  
	
		String s=(play.getName()+ "'s  strategy is "+ play.getStrat().getName() +"");
		showThisMessage( s);   
		return s;
	}
	
	public void determineStart() {
		//random start order maker really just advances turn order to a random spot
		model.randomOrder();
		showThisMessage( "Determines Start");
	}
	
	public String showOrder() {
		
		String mesg = new String();
		
		mesg = mesg.concat("The turn order is: ");
		ArrayList t =model.getTurnOrder();

		for( int i=0;i<t.size();i++)
		{
			mesg=mesg.concat(((Player)t.get(i)).getName()+"  ");
		}
		
		showThisMessage(mesg);
		
		return mesg;
	}
	
	public ReturnStuff resetBoard() {
	
		
		//view.setVerbose(true);
		//showThisMessage( "Clicking here will Reset the board for a new game");
		//view.setVerbose(false);
		
		model.initialize(tileWidth, x, y);
		
		//initialize players to the same names, colors, adn strategies as previous game.
		
		Red.initialize(Red.getColor(), Red.getStrat(), model.getTile(),Red.getName());
		Blue.initialize(Blue.getColor(), Blue.getStrat(), model.getTile(),Blue.getName());
		Green.initialize(Green.getColor(), Green.getStrat(), model.getTile(),Green.getName());          
		Yellow.initialize(Yellow.getColor(), Yellow.getStrat(), model.getTile(),Yellow.getName());
		
//		 Initializing the turn order
		model.addPlayer(Red);                 
		model.addPlayer(Blue);
		model.addPlayer(Green);
		model.addPlayer(Yellow);
		
		determineStart();            //makes it a random starter
		((GUIView) view).preGameMenus();
		return ReturnStuff.good;
		
		
	}
	
	public void setPlayerStrategy(Player p, Strategy s) {  
		p.setStrat(s);																			//does the model part then
		showThisMessage(p.getName() +" is now set to " + s.getName());			//updates the view with a message
		
	}
	
	public ReturnStuff showMove(Player p) {  
		
		showThisMessage("Shows Move");			//will be implemented only when user input is requested and availible, 
		//view.showThisMessage( "Shows Move");
		
		return ReturnStuff.good;
	}
	
	public boolean checkMove(Slot s) {  
		
		//Slot s = p.getStrat().decideMove(model,p);
		
		if (model.getSlot(s.getRowID(), s.getColumnID()).isEmpty())
		{
			showThisMessage( "Valid Move");
			return true;
		}
		else
		{
			//showThisMessage( "INVALID Move");
		}
		return false;
	}
	
	public boolean doMove() 
	{
		Player p = getCurrentPlayer();
		Slot s = p.getStrat().decideMove(this,p);    //move to where it it is picking the move
		boolean r;
		if (checkMove(s))
		{	
			model.getSlot(s.getRowID(), s.getColumnID()).assignTile(s.getTile());
			showThisMessage( "Good Move");
			r=true;
		}
		else
		{
			//showThisMessage( "Can't Do that Move");
			r=false;
		}
		if(s.getTile().getType()=='l')   //if its a modifier type then have at it
		{
			model.updateRowAndColValuesOnSlots(s.getRowID(), s.getColumnID(), s.getTile().getValue());
		}
		model.updateRowAndColFree(s.getRowID(), s.getColumnID());
		model.updateScores();
		model.advanceTurnOrder();
		model.display();
		
		showThisMessage( "Move Done");
		return r;
		
	}
	
	public String displayScore() {
		
		String s = new String();
		
		ArrayList players = model.getTurnOrder();
		Player temp;
		for(int i=0; i<players.size();i++)
		{
			temp=(Player)players.get(i);
			s=s.concat(temp.getName()+ "'s score: "+ temp.getScore() + "\n");
		}
		view.setVerbose(true);
		showThisMessage( s);
		view.setVerbose(false);
		return s;
	}
	
	public String displayWinner() {
		
		Player winner = new Player(Color.BLACK,null,null,"BAD");   //more arbitray initialization badness
		ArrayList play = model.getTurnOrder();
		Player p =((Player) play.get(0));
		int max=p.getScore();           //arbitrarliy low lowest possible score... bad i know
		winner=p;
		for(int i=0; i<play.size();i++)
		{
			p=(Player)play.get(i);
			if(p.getScore()>max)
			{
				max=p.getScore();
				winner=p;
			}
		}
	
		view.setVerbose(true);
		String mesg;
		mesg="The Winner is " + winner.getName();
		showThisMessage( mesg);
		view.setVerbose(false);
		
		return mesg;
	}
	
	public boolean checkFinished() {
		//showThisMessage( "j Finished");
		
		for(int i =0; i<model.getRows();i++)
		{
			for(int j=0; j< model.getColumns();j++)
			{
				if(model.getSlot(i, j).isEmpty())
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	public void doRound() {
		
		//((GUIView) view).setVerbose(false);
		//for(int i=0;i<model.getTurnOrder().size();i++)
			int i=0;
			Move_Result exit = Move_Result.MOVE_COMPLETE;
			while(i<model.getTurnOrder().size() && exit!=Move_Result.PLAYER_WINS)
		{
			exit=doTurn();
			i++;
		}
		
		showThisMessage( "A round should have just been done");
	}
	
	public Move_Result doTurn() {
		
		
		doMove();   		
	
		showThisMessage( "a turn should just have been done");
		
		if (checkFinished())
		{
			next();
			doIt();
			//displayWinner();
			//displayScore();
			//resetBoard();
			return Move_Result.PLAYER_WINS;
		}
		else return Move_Result.MOVE_COMPLETE;
	}
	
	public void doGame() {
		
		Move_Result exit = Move_Result.MOVE_COMPLETE;
		
		while(exit!=Move_Result.PLAYER_WINS)
		{
			exit=doTurn();
		}
		
		showThisMessage( "A whole game should just have been played");
	}
	
	public String viewHand(Player p)
	{
		showThisMessage( "Here is " +p.getName() + "'s  hand, "+ p.getHand());
		return ("Here is " +p.getName() + "'s  hand, "+ p.getHand());
	}
	
	public Player getCurrentPlayer()
	{
		return (Player) model.getTurnOrder().get(0);
	}

	public void draw() {  
		Player p= getCurrentPlayer(); 
		model.draw(p);
		showThisMessage( p.getName()+ " drew a tile");
	}

	public State getGameOn() {
		return gameOn;
	}

	public State getGameOver() {
		return gameOver;
	}

	public State getPreGame() {
		return preGame;
	}

	public void setState(State newState)
	{
		this.state=newState;
	}

	public Player getBlue() {
		return Blue;
	}

	public Player getGreen() {
		return Green;
	}

	public Player getRed() {
		return Red;
	}

	public Player getYellow() {
		return Yellow;
	}

	public void setBlue(Player blue) {
		Blue = blue;
	}

	public void setGreen(Player green) {
		Green = green;
	}

	public void setRed(Player red) {
		Red = red;
	}

	public void setYellow(Player yellow) {
		Yellow = yellow;
	}

	public Board getModel() {
		return model;
	}
	
	public State getState() {
		return  state;
	}

	public GameObserver getView() {
		return view;
	}
	
	public void doIt()
	{
		state.doit();
	}
	
	public void next()
	{
		state.next();
	}
	
	public void lineItUp()
	{
		((GUIView)view).setupPlayersMenus();
	}

	public ReturnStuff showBoard() {
		return model.display();
	}
	
		

}
