package controller;

import java.awt.Color;

import model.BenefitSelf;
import model.Biggest;
import model.DumbStrat;
import model.Human;
import model.HurtsOthers;
import model.Player;
import model.Strategy;
import view.GUIView;

public class PreGame  implements State{
	
	
	GameController gameController;
	
	public PreGame(GameController g)
	{
		this.gameController=g;
	}
	
	public void doit()
	{
		if (gameController.getState() == gameController.getPreGame())
		{
			
			
			gameController.resetBoard();
			//gameController.getView().createDisplay(gameController.getModel().getSlotWidth());
		}
				
	}
	
	
	public void next()
	{
		gameController.setState(gameController.getGameOn());
		gameController.getState().doit();
	}

}
