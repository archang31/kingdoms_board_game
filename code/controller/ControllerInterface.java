package controller;

import model.Move_Result;
import model.Player;
import model.Slot;
import model.Strategy;

public interface ControllerInterface {

	ReturnStuff showBoard();
	
	/*void placeToken(Color tokenColor, int x, int y);*/
	
	String viewHand(Player play);
	
	String showStrategy(Player play);
	
	void draw();

	void determineStart();
	
	String showOrder();
	
	
	
	ReturnStuff resetBoard();
	/*
	void resetLandTiles();
	
	void resetCounters();
	*/
	ReturnStuff newGame();
	
	void setPlayerStrategy(Player p, Strategy s);
	
	ReturnStuff showMove(Player play);
	
	//void checkMove(char p, Tile t, Slot s);
	boolean checkMove(Slot s);
	
	//void doMove(char p, Tile t, Slot s);
	boolean doMove();
	
	String displayScore();
	
	String displayWinner();
	
	boolean checkFinished();
	
	void doRound();
	
	Move_Result doTurn();
	
	void doGame();
	
	Player getCurrentPlayer();
	
	
}
