package controller;

public class GameOver  implements State{
	
	GameController gameController;
	
	public GameOver(GameController g)
	{
		this.gameController=g;
	}
	
	
	public void doit()
	{
		if (gameController.getState() == gameController.getGameOver())
		{
			gameController.displayWinner();
			gameController.displayScore();
			next();
		}
		
	}
	
	public void next()
	{
		gameController.setState(gameController.getPreGame());
		gameController.getState().doit();
	}

}
