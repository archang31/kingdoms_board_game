package controller;

import view.GUIView;

public class GameOn  implements State{
	
	GameController gameController;
	
	public GameOn(GameController g)
	{
		this.gameController=g;
	}
	
	public void doit()
	{
		if (gameController.getState() == gameController.getGameOn())
		{
			((GUIView)gameController.getView()).playModeActivated();
			((GUIView) gameController.getView()).myDynamicMenus(); //magic ;
		}
	}
	

	
	public void next()
	{
		gameController.setState(gameController.getGameOver());
	}

}
