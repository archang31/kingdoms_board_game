package controller;

import java.awt.Color;
import java.util.ArrayList;

import sun.awt.windows.ThemeReader;
import view.GUIView;
import view.GameObserver;
import view.TextView;

import junit.framework.TestCase;
import model.Player;
import model.Slot;
import model.Tile;


//These are mostly essenaitll all jsut displaying that the result happened, by aserting (this,this) it lets you walk through the program as it happens
//and see the messages pop up and the nessecary conditons change due to the output of the mesages.  It will fail if there is an excpetion thorwn I believe

public class ControllerTest extends TestCase {
	model.Board b = new model.Board(30,5,6);
	
	GameObserver view1 = new GUIView(b);
	GameObserver view2 = new TextView(b);
	
	GameController cont= new GameController(b,30,5,6);
	
	Player Red = b.getPlayerWithName("Red Player");           //got me some players now
	Player Blue = b.getPlayerWithName("Blue Player");
	Player Green = b.getPlayerWithName("Green Player");
	Player Yellow = b.getPlayerWithName("Yellow Player");
	
	public void testShowBoard() {
		ReturnStuff r =cont.showBoard();
		assertEquals(r, ReturnStuff.good);
	}
	
	
	public void testViewHand() throws Exception {
		String s;
		s=cont.viewHand(Red);
		
		assertEquals(s.substring(8, 8+Red.getName().length()),Red.getName());
		//assertEquals(expected, actual)   41
		assertEquals(true,s.contains( Integer.toString(((Tile)Red.myCastlesList().get(0)).getValue())));
		s=cont.viewHand(Blue);
		assertNotNull(s);				//if ti works for red and blue it should work for the others
		s=cont.viewHand(Green);
		assertNotNull(s);
		s=cont.viewHand(Yellow);
		assertNotNull(s);
	}
	
	public void testShowStrategy() throws Exception {
		String s;
		s=cont.showStrategy(Red);
		assertTrue(s.contains((Red.getStrat().getName())));
		s=cont.showStrategy(Blue);
		assertTrue(s.contains((Blue.getStrat().getName())));

	}
	
	public void testDraw() throws Exception {
		Player p=cont.getCurrentPlayer();
		int cardnum;
		p.myLandList().trimToSize();
		cardnum=p.myLandList().size();
		cont.draw();		//draws a card 
		assertTrue(cont.getCurrentPlayer()==p);
		cont.getCurrentPlayer().myLandList().trimToSize();
		assertTrue(cont.getCurrentPlayer().myLandList().size()==cardnum+1);
	}

	
	public void testShowOrder() throws Exception {
		Player p = cont.getCurrentPlayer();
		String s=cont.showOrder();//18
		//view1.setVerbose(true);
		//view1.showThisMessage(s.substring(19,19+p.getName().length()+1));
		//view1.setVerbose(false);
		assertEquals(s.substring(19,19+p.getName().length()), p.getName());
	}
	
	public void testAdvanceOrder() throws Exception {
		Player p =cont.getCurrentPlayer();
		b.advanceTurnOrder();
		assertNotSame(p, cont.getCurrentPlayer());
		b.advanceTurnOrder();
		assertNotSame(p, cont.getCurrentPlayer());
		b.advanceTurnOrder();
		assertNotSame(p, cont.getCurrentPlayer());
		b.advanceTurnOrder();  //if i advance turn order 4 times it will be bakc to the big guy up top
		assertEquals(p, cont.getCurrentPlayer());

		assertEquals(this, this);
	}
	
	public void testResetBoard() throws Exception {
		//cont.doMove();
		Tile t= new Tile('e',0,Color.CYAN, "pics/empty.jpg");
		((Slot)((ArrayList<Slot>)b.getTheBoard().get(0)).get(0)).assignTile(t);
		assertTrue(((Color)((Tile)(((Slot)((ArrayList<Slot>)b.getTheBoard().get(0)).get(0))).getTile()).getColor())==Color.CYAN);
		cont.resetBoard();
		assertNotSame((((Slot)((ArrayList<Slot>)b.getTheBoard().get(0)).get(0)).getTile()),t);
	}
	
/*	public void testResetLandTiles() throws Exception {                //well bugger, we've gone and gotten rid of these ol chaps
		cont.resetLandTiles();
		assertEquals(this, this);
	}	
	
	public void testResetCounters() throws Exception {
		cont.resetCounters();
		assertEquals(this, this);
	}
	*/
	
	
	public void testNewGame() throws Exception {
		ReturnStuff r=cont.newGame();
		assertEquals(r, ReturnStuff.good);
		
		cont.doRound();
		cont.doRound();
		cont.doRound();
			
		 r=cont.newGame();
		assertEquals(r, ReturnStuff.good);
		
		Red.setScore(7);
		 r=cont.newGame();
		 assertEquals(r, ReturnStuff.good);
		 assertTrue(Red.getScore()==0);
		
		
	
		
	}
	
	public void testSetPlayerStrategy() throws Exception {
		cont.setPlayerStrategy(Red, new model.BenefitSelf());
		cont.setPlayerStrategy(Blue, new model.BenefitSelf());
		cont.setPlayerStrategy(Green, new model.BenefitSelf());
		cont.setPlayerStrategy(Yellow, new model.BenefitSelf());
		assertEquals(this, this);
	}
	public void testShowMove() throws Exception {
		ReturnStuff r = cont.showMove(Red);
		assertEquals(r, ReturnStuff.good);
		 r = cont.showMove(Blue);
		 assertEquals(r, ReturnStuff.good);
		 r = cont.showMove(Green);
		 assertEquals(r, ReturnStuff.good);
		 r = cont.showMove(Yellow);
		 assertEquals(r, ReturnStuff.good);
		
	}
	
	public void testCheckMove() throws Exception {
		Slot s= new Slot(0,0);
		Boolean a=cont.checkMove(s);		//should be true
		Tile t= new Tile('c',2,Color.RED, "pics/castler2.jpg");
		((Slot)((ArrayList<Slot>)b.getTheBoard().get(0)).get(0)).assignTile(t);
		Boolean b= cont.checkMove(s);		//should be false casue i jsut  put one there HAAaA
		
	
		assertNotSame(a, b);
	}
	
	public void testDoMove() throws Exception {
		int bef=b.numOfEmpty();
		boolean r= cont.doMove();
		int af=b.numOfEmpty();
		if(bef==af)
		{
			assertTrue(r==false);
		}
		else
		{
			assertTrue(r==true);
		}
	}
	
	public void testDisplayScore() throws Exception {
		String s=cont.displayScore();
		assertTrue(s.length()>0);
		assertTrue(s.contains("Blue"));
		assertTrue(s.contains("Red"));
		assertTrue(s.contains("Yellow"));
		assertTrue(s.contains("Green"));
	}
	
	public void testDisplayWinner() throws Exception {
		
		String in=cont.displayWinner();
		
		Player winner=null;
		ArrayList play = b.getTurnOrder();
		Player p;
		int max=-999999999;           //arbitrarliy low lowest possible score... bad i know
		for(int i=0; i<b.getTurnOrder().size();i++)
		{
		
			p=(Player)b.getTurnOrder().get(i);
			if(p.getScore()>max)
			{
				max=p.getScore();
				winner=p;
			}
		}
		
		assertTrue(in.contains(winner.getName()));
	}
	
	public void testCheckFinished() throws Exception {
		cont.checkFinished();
		assertTrue(b.numOfEmpty()==b.getColumns()*b.getRows());
	}
	
	public void testDoRound() throws Exception {
		Player p = cont.getCurrentPlayer();
		int sum=0;
		sum=b.numOfEmpty();
		cont.doRound();		//a round places 4 tokens on ther board, one for each player
		assertEquals(sum-4, b.numOfEmpty());
	
		Player p1=cont.getCurrentPlayer();
		
		assertTrue(p.getName()==p1.getName());
	}
	
	public void testDoTurn() throws Exception {
		
		ArrayList a= b.getTurnOrder();
		Player p = (Player) a.get(1);
		int sum=0;
		sum=b.numOfEmpty();
		cont.doTurn();						//does one turn
		assertEquals(sum-1, b.numOfEmpty());
		assertEquals(p.getName(), ((Player)b.getTurnOrder().get(0)).getName());
	}
	
	public void testDoGame() throws Exception {
		cont.doGame();
		int sum= b.numOfEmpty();
		assertEquals(sum, b.getColumns()*b.getRows());
	}
}
