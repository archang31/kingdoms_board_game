package model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class StackOTiles {
	
	private ArrayList<Tile> deck;
	private Random rand= new Random();
	
	public StackOTiles()
	{
		deck = new ArrayList(18);
	}
	
	public Tile drawTile()
	{
		if ( !deck.isEmpty())
		{
			return deck.remove(rand.nextInt(deck.size()));
		}
		else
		{
			return null;
		}
	}

}
