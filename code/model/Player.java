package model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;

import model.DumbStrat;

public class Player {
	
	
	private Color playerColor ;
	private Strategy strat;
	private ArrayList<Tile> landTiles;
	private ArrayList<Tile> castleTiles;     //broke it into 2 becuase castels are seprate form land
	private int score;
	private String name;
	private int MAX_CASTLE_VALUE = 4;
	private int MAX_LAND_VALUE = 6;

	public Player(Color aColor, Strategy aStrat,Tile t, String aName)
	{	
		initialize(aColor,aStrat,t,aName);
	}
		
	public void initialize(Color aColor, Strategy aStrat,Tile t, String aName)
	{
		score = 0;
		strat = aStrat;
		char p = ' ';
		landTiles = new ArrayList(2);
		castleTiles = new ArrayList(8);
		if (aColor ==  Color.RED){
			p = 'r';
		}
		else if (aColor == Color.YELLOW){
			p = 'y';
		}
		else if (aColor == Color.BLUE){
			p = 'b';
		}
		else if (aColor == Color.GREEN){
			p = 'g';
		}
		
		castleTiles.add(new Tile('c',1,aColor, "pics/castle" + p + "1.jpg"));                 //initialize thme off with thier 8 castles
		castleTiles.add(new Tile('c',1,aColor, "pics/castle" + p + "1.jpg"));
		castleTiles.add(new Tile('c',2,aColor, "pics/castle" + p + "1.jpg"));
		castleTiles.add(new Tile('c',2,aColor, "pics/castle" + p + "2.jpg"));
		castleTiles.add(new Tile('c',2,aColor, "pics/castle" + p + "2.jpg"));
		castleTiles.add(new Tile('c',3,aColor, "pics/castle" + p + "3.jpg"));
		castleTiles.add(new Tile('c',3,aColor, "pics/castle" + p + "3.jpg"));
		castleTiles.add(new Tile('c',4,aColor, "pics/castle" + p + "4.jpg"));
		
		landTiles.add(t);   //give thme thier inital land tile
		name = aName;
		playerColor=aColor;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getScore()
	{
		return score;
	}
	
	public void setScore(int aScore)
	{
		score = aScore;
	}

	public Color getColor()
	{
		return playerColor;
	}

	public Strategy getStrat() {
		return strat;
	}

	public void setStrat(Strategy s) {
			
		this.strat = s;
	}
	
	public void grabTileFromStack(Tile t)
	{
		landTiles.add(t);
	}
	
	public void grabCastleFromStack(Tile t)
	{
		castleTiles.add(t);
	}
	
	public Tile getCastle(int value){
		Tile t = new Tile('e', 0,Color.BLACK, "pics/empty.jpg"); // this is bad, but I know it will not be empty
		for(int i=0; i<castleTiles.size(); i++)
		{
			if (castleTiles.get(i).getValue() == value){
				t = castleTiles.get(i);
				castleTiles.remove(i);
				return t;
			}
		}
		return t;
	}
	
	public Tile getCastleByIndex(int index){
		Tile t = new Tile('e', 0,Color.BLACK, "pics/empty.jpg"); // this is bad, but I know it will not be empty
		t = castleTiles.get(index);
		castleTiles.remove(index);
		return t;
	}
	
	public Tile onlySeeLand(int index){
		Tile t = new Tile('e', 0,Color.BLACK, "pics/empty.jpg"); // this is bad, but I know it will not be empty
		t = landTiles.get(index);
		return t;
	}
	
	public Tile onlySeeCastle(int index){
		Tile t = new Tile('e', 0,Color.BLACK, "pics/empty.jpg"); // this is bad, but I know it will not be empty
		t = castleTiles.get(index);
		return t;
	}
	
	public Tile getLand(int value){
		Tile t = new Tile('e', 0,Color.BLACK, "pics/empty.jpg"); // this is bad, but I know it will not be empty
		for(int i=0; i<landTiles.size(); i++)
		{
			if (landTiles.get(i).getValue() == value){
				t = landTiles.get(i);
				landTiles.remove(i);
				return t;
			}
		}
		return t;
	}
	
	public Tile getLandByIndex (int index){
		Tile t = new Tile('e', 0,Color.BLACK, "pics/empty.jpg"); // this is bad, but I know it will not be empty
		t = landTiles.get(index);
		landTiles.remove(index);
		return t;
	}
	
	
	public String getHand()
	{
		String hand= new String(" land tiles:");
		
		for(int i=0; i<landTiles.size(); i++)
		{
			hand=hand.concat(" "+landTiles.get(i).getValue() + ",");
		}

		hand=hand.concat("  castle tiles:");
		for(int i=0; i<castleTiles.size(); i++)
		{
			hand=hand.concat(" "+castleTiles.get(i).getValue() + ",");
		}
		
		return hand;
	}
	
	public int maxCastle(int max){
		for (int i=max; i>=1; i--){
			if (hasCastle(i)){
				return i;
			}
		}
		return MAX_CASTLE_VALUE+1;
	}
	
	public int minCastle(){
		for (int i=1; i<=MAX_CASTLE_VALUE; i++){
			if (hasCastle(i)){
				return i;
			}
		}
		return MAX_CASTLE_VALUE+1;
	}
	
	public int maxLand(){
		for(int i=MAX_LAND_VALUE; i>=-MAX_LAND_VALUE; i--)
		{
			if (hasLand(i)){
				return i;
			}
		}
		return MAX_LAND_VALUE+1;
	}
	
	public int minLand(){
		for(int i=-MAX_LAND_VALUE; i<=MAX_LAND_VALUE; i++)
		{
			if (hasLand(i)){
				return i;
			}
		}
		return MAX_LAND_VALUE+1;
	}
	
	public boolean hasCastle(int value){
		if (!castleIsEmpty())
		{
			for(int i=0; i<castleTiles.size(); i++)
				if (castleTiles.get(i).getValue() == value){
					return true;
				}
		}
		return false;
	}
	
	public boolean hasLand(int value){
		if (!landIsEmpty())
		{
			for(int i=0; i<landTiles.size(); i++)
				if (landTiles.get(i).getValue() == value){
					return true;
				}
		}
		return false;
	}
		
	public boolean castleIsEmpty(){
		return castleTiles.isEmpty();
	}
	
	public int castleArraySize(){
		return castleTiles.size();
	}
	
	public boolean landIsEmpty(){
		return landTiles.isEmpty();
	}
	
	public int landArraySize(){
		return landTiles.size();
	}
	
	public ArrayList myLandList(){
		return landTiles;
	}
	
	public ArrayList myCastlesList(){
		return castleTiles;
	}
	
	public void removeTile(Tile t){
		if (t.getType() == 'l'){
			for(int i=0; i<landTiles.size(); i++)
				if (landTiles.get(i).getValue() == t.getValue()){
					landTiles.remove(i);
				}
			}
		else if (t.getType() == 'c'){
			for(int i=0; i<castleTiles.size(); i++)
			{
				if (castleTiles.get(i).getValue() == t.getValue()){ 
					castleTiles.remove(i);
				}
			}
		}
	}
	
	public void setName(String s)
	{
		this.name=s;
	}
	
	public ArrayList getLandArray()
	{
		return 	landTiles;
	}
	
	public ArrayList getCastleArray()
	{
		return 	castleTiles;
	}
	
	
}

