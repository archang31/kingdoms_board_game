package model;

import controller.GameController;

public class Biggest extends BaseStrategy implements Strategy{
	private String name;
		
		public Biggest()
		{
			this.name = "Biggest";
		}
	
		public String getName()
		{
			return this.name;
		}
		
		public Slot decideMove(GameController g, Player p)
		{		
			Board b = g.getModel();
			Slot s = new Slot (3,3);
			if (!p.castleIsEmpty() && !p.landIsEmpty()){
				if ((2*(p.maxCastle(4)-1)) >= p.maxLand()){
					s = maxLandSum(b);
					if (s.getTotSum() <= 0){
						s = randomSlot(b);
					}
					s.assignTile(p.getCastle(p.maxCastle(4)));
				}
				else{			
					b.draw(p);
					s = maxPosCasSlot(b, p);
					s.assignTile(p.getLand(p.maxLand()));
				}
				
			}	
			else if (!p.castleIsEmpty()){
				s = maxLandSum(b);
				if (s.getTotSum() <= 0){
					s = randomSlot(b);
				}
				s.assignTile(p.getCastle(p.maxCastle(4)));
			}
			else{
				b.draw(p);
				s = maxPosCasSlot(b, p);
				s.assignTile(p.getLand(p.maxLand()));
			}
			return s;	
		}
	
}
