package model;

import java.util.Random;

import controller.GameController;

public class DumbStrat extends BaseStrategy implements Strategy {
	
	String name;
	
	public DumbStrat()
	{
		this.name= "Dumb Strategy";
	}
	
	public Slot decideMove(GameController g, Player p) 
	{
		Board b = g.getModel();
		Slot imag = new Slot(0,0);
		imag = randomSlot(b);
		int i = b.getRandInt(2);
		if (i == 0){
			b.draw(p);
			if (!p.landIsEmpty()){
				imag.assignTile(randomLand(b, p));
			}
			else
			{
				imag.assignTile(randomCastle(b, p));
			}
		}
		else if (i == 1){
			if (!p.castleIsEmpty()){
				imag.assignTile(randomCastle(b, p));
			}
			else
			{
				b.draw(p);
				imag.assignTile(randomLand(b, p));
			}
		}
		return imag;
	}

	public String getName() {
		return name;
	}

}
