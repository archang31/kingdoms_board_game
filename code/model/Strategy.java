package model;

import controller.GameController;

public interface Strategy{
	
	public String getName();
	
	public Slot decideMove(GameController controller,Player p);

}
