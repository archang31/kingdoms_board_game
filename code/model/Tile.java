package model;

import java.awt.Color;

public class Tile {

	private char type;					//c = castle   l=land  e = empty
	private int value;
	private Color c;
	private String fileName;
	
	public Tile(char atype,int avalue, Color aColor, String fn)
	{
		type = atype;
		value = avalue;
		c =aColor;
		fileName = fn;
	}

	public char getType() {
		return type;
	}

	public int getValue() {
		return value;
	}
	
	public Color getColor()
	{
		return c;
	}
	
	public String getFileName()
	{
		return fileName;
	}
}
