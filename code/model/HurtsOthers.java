package model;

import java.util.ArrayList;
import java.util.List;

import controller.GameController;

public class HurtsOthers extends BaseStrategy implements Strategy{
	private String name = "Hurts Others";
		
		public HurtsOthers()
		{

		}
	
		public String getName()
		{
			return this.name;
		}
		
		public Slot decideMove(GameController g, Player p)
		{
			Board b = g.getModel();
			Slot s = new Slot(0,0);
			if (!p.landIsEmpty()){
				if (p.minLand() < 0){
					s = maxNegCasSlot(b, p);
					if (getTotalSum() > 0){
						b.draw(p);
						s.assignTile(p.getLand(p.minLand()));
					}
					else {
						s = maxLandSum(b);
						s.assignTile(p.getCastle(p.maxCastle(4)));
					}
				}
				else if (p.minLand() <= 2){
					s = maxLandSum(b);
					b.draw(p);
					s.assignTile(p.getLand(p.minLand()));
				}
				else if (!p.castleIsEmpty()){
					s = maxLandSum(b);
					s.assignTile(p.getCastle(p.maxCastle(4)));
					
				}
				else {
					b.draw(p);
					s = maxPosCasSlot(b, p);
					s.assignTile(p.getLand(p.maxLand()));
				}
			}		
			return s;
		}
	
}
