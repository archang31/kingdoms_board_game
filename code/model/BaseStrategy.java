package model;

public class BaseStrategy {
	private int maxTotalSum = -999;
	
	public int getTotalSum(){
		return maxTotalSum;
	}
	
	public Slot maxLandSum(Board b){
		Slot d = new Slot(0,0);
		int curMax = -999;
		for (int i=b.getRows()-1; i >= 0; i--)
		{
			for (int j=b.getColumns()-1; j>= 0; j--)
			{
				if ((b.getSlot(i, j)).isEmpty()){
					if (b.getSlot(i, j).getTotSum() > curMax){
						curMax = b.getSlot(i, j).getTotSum();
						d = (b.getSlot(i, j));
					}
				}
			}
		}
		System.out.println("The current total sum is "+ d.getTotSum() +".");
		return d;
	}
	
	public Slot maxNegCasSlot(Board b, Player p)
	{
		Slot d = new Slot(0,0);
		int [] rows =new int[b.getRows()];
		int [] cols =new int[b.getColumns()];
		if (!p.landIsEmpty()){
			if (p.minLand() < 0){
				for (int i=0; i<b.getRows(); i++)
				{
					int rowSum = 0;
					for (int j=0; j< b.getColumns(); j++)
					{
						if (((b.getSlot(i, j)).getTile()).getType() == 'c'){
							if (b.getSlot(i, j).getTile().getColor() == p.getColor()){
								rowSum = rowSum - 3*b.getSlot(i, j).getTile().getValue();
							}
							else
							{
								rowSum = rowSum + b.getSlot(i, j).getTile().getValue();
							}
								
						}
					}
					rows[i] = rowSum;
				}
				for (int j=0; j< b.getColumns(); j++)
				{
					int colSum = 0;
					for (int i=0; i<b.getRows(); i++)
					{
						if (((b.getSlot(i, j)).getTile()).getType() == 'c'){
							if (b.getSlot(i, j).getTile().getColor() == p.getColor()){
								colSum = colSum - 3*b.getSlot(i, j).getTile().getValue();
							}
							else
							{
								colSum = colSum + b.getSlot(i, j).getTile().getValue();
							}
						}
					}
					cols[j] = colSum;
				}
				int maxSum = -999;
				for (int i=0; i<b.getRows(); i++)
				{

					for (int j=0; j< b.getColumns(); j++)
					{
						if (rows[i]+cols[j] > maxSum){
							if (b.getSlot(i, j).isEmpty()) {
								maxSum = rows[i]+cols[j];
								maxTotalSum = maxSum;
								d = b.getSlot(i, j).copy();
							}
						}
					}
				}
				
			}
		}
		return d;
	}
	
	public Slot maxPosCasSlot(Board b, Player p)
	{
		Slot d = new Slot(0,0);
		int [] rows =new int[b.getRows()];
		int [] cols =new int[b.getColumns()];
		if (!p.landIsEmpty()){
			if (p.minLand() < 0){
				for (int i=0; i<b.getRows(); i++)
				{
					int rowSum = 0;
					for (int j=0; j< b.getColumns(); j++)
					{
						if (((b.getSlot(i, j)).getTile()).getType() == 'c'){
							if (b.getSlot(i, j).getTile().getColor() == p.getColor()){
								rowSum = rowSum + 3*b.getSlot(i, j).getTile().getValue();
							}
							else
							{
								rowSum = rowSum - b.getSlot(i, j).getTile().getValue();
							}
								
						}
					}
					rows[i] = rowSum;
				}
				for (int j=0; j< b.getColumns(); j++)
				{
					int colSum = 0;
					for (int i=0; i<b.getRows(); i++)
					{
						if (((b.getSlot(i, j)).getTile()).getType() == 'c'){
							if (b.getSlot(i, j).getTile().getColor() == p.getColor()){
								colSum = colSum - 3*b.getSlot(i, j).getTile().getValue();
							}
							else
							{
								colSum = colSum + b.getSlot(i, j).getTile().getValue();
							}
						}
					}
					cols[j] = colSum;
				}
				int maxSum = -999;
				for (int i=0; i<b.getRows(); i++)
				{

					for (int j=0; j< b.getColumns(); j++)
					{
						if (rows[i]+cols[j] > maxSum){
							if (b.getSlot(i, j).isEmpty()) {
								maxSum = rows[i]+cols[j];
								maxTotalSum = maxSum;
								d = b.getSlot(i, j).copy();
							}
						}
					}
				}
			}
		}
		return d;
	}
	
	public Slot randomSlot(Board b){
		Slot n = new Slot(b.getRandInt(b.getRows()),b.getRandInt(b.getColumns()));
		while(!b.getSlot(n.getRowID(), n.getColumnID()).isEmpty())
		{
			n = (b.getSlot(b.getRandInt(b.getRows()),b.getRandInt(b.getColumns()))).copy();
		}
		return n;
	}
	
	public Tile randomCastle(Board b, Player p){
		return p.getCastleByIndex(b.getRandInt(p.castleArraySize()));
	}
	
	public Tile randomLand(Board b, Player p){
		return p.getLandByIndex(b.getRandInt(p.landArraySize()));
	}
	/**
	public Slot assignTile(Player p, Slot s, Tile t){
		s.assignTile(t);
		p.removeTile(t);
		return s;
	}
	*/
}
