package model;

import view.GUIView;
import controller.GameController;


public class Human extends BaseStrategy implements Strategy {
	private String name;
		
		public Human()
		{
			 this.name = "Human player";
		}
	
	
		public String getName()
		{
			return this.name;
		}
		
		public Slot decideMove(GameController g, Player p)
		{
			Slot s = new Slot(0,0);
			int newValue = 0;
			int curMax = -999;
			
			((GUIView) g.getView()).userInput(p,s);
			
			System.out.println(s.getRowID());
			System.out.println(s.getColumnID());
			System.out.println(s.getTile().getType());
			System.out.println(s.getTile().getValue());
			return s;
		}
}
