package model;

import controller.GameController;


public class BenefitSelf extends BaseStrategy implements Strategy {
	private String name;
		
		public BenefitSelf()
		{
			 this.name = "Benefits Self";
		}
	
	
		public String getName()
		{
			return this.name;
		}
		
		public Slot decideMove(GameController g, Player p)
		{
			Board b = g.getModel();
			Slot s = new Slot(0,0);
			int newValue = 0;
			int curMax = -999;
			for (int i=0; i<b.getRows(); i++)
			{
				for (int j=0; j< b.getColumns(); j++)
				{
					if ((b.getSlot(i, j)).isEmpty()){
						int sum = (b.getSlot(i, j)).getTotSum();
						int free = (b.getSlot(i, j)).getTotFree();
						newValue = ((b.getSlot(i, j)).getTotSum() - 2*(b.getSlot(i, j)).getTotFree());
						if (newValue > curMax){
							s = (b.getSlot(i, j)).copy();
							curMax = newValue;
						}
					}
				}
			}
			if (!p.castleIsEmpty() && curMax > 0){
				if (curMax > 12){
					s.assignTile(p.getCastle(p.maxCastle(4)));
				}
				else if (curMax > 8){
					s.assignTile(p.getCastle(p.maxCastle(3)));
				}
				else if (curMax > 4){
					s.assignTile(p.getCastle(p.maxCastle(2)));
				}
				else if (curMax >= 1){
					s.assignTile(p.getCastle(p.maxCastle(1)));
				}
			}
			else {
				if (!b.stackOfTilesIsEmpty()){
					b.draw(p);
				}
				if (!p.landIsEmpty()){
					s = randomSlot(b);
					s.assignTile(p.getLand(p.maxLand()));
				}
				else
				{
					s.assignTile(p.getCastle(p.minCastle()));
				}
			}
			return s;
		}
}
