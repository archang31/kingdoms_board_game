package model;

public enum Move_Result {

	MOVE_COMPLETE, TIE_GAME, PLAYER_WINS, BAD_MOVE;
}
