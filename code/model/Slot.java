package model;

import java.awt.Color;



public class Slot {

	private int rowID;
	private int columnID;
	private Tile type; 
	private int rowSum;
	private int colSum;
	private int rowFree;
	private int colFree;
	
	
	public Slot(int rowID, int columnID) {
		this.rowID = rowID;
		this.columnID = columnID;
		type = null;
		rowSum=0;
		colSum=0;
		rowFree=6;
		colFree=5;
	}

	public void setRowSum(int sum)
	{
		rowSum=sum;
	}
	public void setColSum(int sum)
	{
		colSum=sum;
	}
	
	public void setRowFree(int free)
	{
		rowFree=free;
	}
	public void setColFree(int free)
	{
		colFree=free;
	}
	
	public int getRowSum()
	{
		return rowSum;
	}
	public int getColSum()
	{
		return colSum;
	}
	
	public int getRowFree()
	{
		return rowFree;
	}
	public int getColFree()
	{
		return colFree;
	}
	public int getTotSum()
	{
		return (colSum + rowSum);
	}
	
	public int getTotFree()
	{
		return (colFree + rowFree);
	}
	
	public void setTile(Tile t)
	{
		type = t;
	}
	
	public void setX(int xcord)
	{
		rowID=xcord;
	}
	public void setY(int ycord)
	{
		columnID=ycord;
	}
	
	public boolean isEmpty() {
		if (type.getType() == 'e')
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public void setEmpty(boolean empty) {
		this.type = new model.Tile('e',0,Color.BLACK, "pics/empty.jpg");
	}

	public int getColumnID() {
		return columnID;
	}

	public int getRowID() {
		return rowID;
	}
	
	public void assignTile(Tile t)
	{
		type = t;
		
	}
	
	public Tile getTile()
	{
		return type;
	}
	
	
	public Slot copy(){
		Slot s = new Slot(0,0);
		s.setX(getRowID());
		s.setY(getColumnID());
		Tile t = new Tile((getTile().getType()), (getTile().getValue()), (getTile().getColor()), (getTile().getFileName()));
		s.assignTile(t);
		return s;
	}
}
