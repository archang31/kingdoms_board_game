package model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import controller.ReturnStuff;

import view.GameObserver;

public class Board{

	ArrayList gameObservers = new ArrayList();
	private int rows;
	private int columns;
	private int slotWidth;
	private List theBoard;
	private ArrayList<Tile> stackOfTiles = new ArrayList<Tile>(18);
	private Random rand;
	private ArrayList<Player> turnOrder= new ArrayList<Player>();
	ArrayList<Strategy> availibleStrats = new ArrayList<Strategy>();
	private Player currentPlayer;
	private int MAX_LAND_VALUE = 6;
	
	
	public Board(int slotWidth, int rows, int cols) {
		this.slotWidth = slotWidth;
		this.rows = rows;
		this.columns = cols;
		rand = new Random();
		initialize(slotWidth, rows, cols);		
	}
	
	public void addStrategy(Strategy b)
	{
		availibleStrats.add(b);
	}
	
	public void addPlayer(Player p)
	{
		turnOrder.add(p);
	}
	
	public ArrayList getObservers()
	{
		return gameObservers;
	}
	
	public ArrayList getTurnOrder()
	{
		return turnOrder;
	}
	public void advanceTurnOrder()
	{
		ArrayList<Player> newOrder = new ArrayList<Player>();
		
		newOrder.add(turnOrder.get(1));
		newOrder.add(turnOrder.get(2));
		newOrder.add(turnOrder.get(3));
		newOrder.add(turnOrder.get(0));
		turnOrder=newOrder;
		currentPlayer = turnOrder.get(0);
		//return newOrder;
	}
	
	public int getColumns() {
		return columns;	
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public int getRows() {
		return rows;
	}
	

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getSlotWidth() {
		return slotWidth;
	}

	public void setSlotWidth(int SlotWidth) {
		this.slotWidth = SlotWidth;
	}

	public Slot getSlot(int x, int y) {
		if(x<getRows() && y < getColumns() )
		{
			return (Slot)((ArrayList)theBoard.get(x)).get(y);
		}
		System.out.println("BAD, BAD, BAD, why are you askign for a slot that dosent exist:");      //??????????? yeah i know its from before when soem major badness was hapneing ona regualr basis
		System.out.println("you asked for x: "+ x+"    y: "+y);
		Slot BAD = new Slot(x,y);
		BAD.assignTile(new Tile('e',0,Color.PINK, "pics/bad.jpg"));
		return BAD;
	}


	public void initialize(int slotWidth, int x, int y) {
		
		theBoard = new ArrayList<Slot>(rows);
		
		
		//Initializing the 2 dimensional array of slots that is the board
		for (int i=0; i<rows; i++)
		{
			theBoard.add(i, new ArrayList<Slot>(columns));
			for (int j=0; j<columns; j++)
			{
				Slot temp = new Slot(i,j);
				temp.assignTile(new Tile('e',0,Color.BLACK, "pics/empty.jpg"));
				((ArrayList)theBoard.get(i)).add(j, temp);
			}
		}
		
		// Initializing the stack of tiles
		for (int i = 1; i <= MAX_LAND_VALUE; i++) 
		{
			stackOfTiles.add(new Tile('l',i,Color.BLACK, "pics/land" + Integer.toString(i) + ".jpg"));      //add 2 land tiles for each value
			stackOfTiles.add(new Tile('l',i,Color.BLACK, "pics/land" + Integer.toString(i) + ".jpg"));
			stackOfTiles.add(new Tile('l',-i,Color.BLACK, "pics/landn" + Integer.toString(i) + ".jpg"));		//and a hazard
		}	
		
		while(!turnOrder.isEmpty())
		{
			turnOrder.remove(0);
		}
	}

	public Tile getTile()
	{
		if ( !stackOfTiles.isEmpty())
		{
			return stackOfTiles.remove(rand.nextInt(stackOfTiles.size()));
		}
		else
		{
			return null;
		}
	}
	
	public void draw(Player p) 
	{  
		if (!stackOfTiles.isEmpty()){
			p.grabTileFromStack(getTile());
		}
	}

	public void placeToken(int x, int y, Color tokenColor) {
		// TODO Auto-generated method stub
		
	}
	
	public ReturnStuff display() {
		
		ReturnStuff r;
		int count =0;
		for(int i=0; i<gameObservers.size(); i++)
		{
			GameObserver observer = (GameObserver)gameObservers.get(i);
			r=observer.display();
			if(r==ReturnStuff.good)
			{
				count=count+1;
			}
		}
		showBoard();
		if (count==gameObservers.size())
		{
			return ReturnStuff.good;
		}
		return ReturnStuff.bad;
	}

	public void removeObserver(GameObserver o) {
		int i = gameObservers.indexOf(o);
		if(i >= 0){
			gameObservers.remove(i);
		}		
	}

	public void showBoard()
	{
		clear();
		//drawColumnLabels(columns);
		//drawRowLabels(rows);	
		drawBackground();
		for (int i=0; i<rows; i++)
		{
			
			for (int j=0; j<columns; j++)
			{
				Slot thisSlot = getSlot(i,j);
				if (thisSlot.isEmpty() == false)
				{
					drawOccupiedSlot(j+1,i+1,thisSlot.getTile());
				}
			}
		}
		drawPlayerHand();
		//display();
	}

	public void registerObserver(GameObserver o) {
		gameObservers.add(o);
	}

	public void clear(){
		for(int i=0; i<gameObservers.size(); i++)
		{
			GameObserver observer = (GameObserver)gameObservers.get(i);
			observer.clear();
		}
	}
	
	public void drawBackground(){
		for(int i=0; i<gameObservers.size(); i++)
		{
			GameObserver observer = (GameObserver)gameObservers.get(i);
			observer.drawBackground();
		}
	}
	
	public void drawPlayerHand(){
		for(int i=0; i<gameObservers.size(); i++)
		{
			GameObserver observer = (GameObserver)gameObservers.get(i);
			observer.drawPlayerHand(currentPlayer);
		}
	}
	
	public void drawColumnLabels(int numColumns) {
		for(int i=0; i<gameObservers.size(); i++)
		{
			GameObserver observer = (GameObserver)gameObservers.get(i);
			observer.drawColumnLabels(numColumns);
		}
	}

	public void drawRowLabels(int numRows) {
		for(int i=0; i<gameObservers.size(); i++)
		{
			GameObserver observer = (GameObserver)gameObservers.get(i);
			observer.drawRowLabels(numRows);
		}	
	}

	public void drawOccupiedSlot(int x, int y, Tile t) {
		for(int i=0; i<gameObservers.size(); i++)
		{
			GameObserver observer = (GameObserver)gameObservers.get(i);
			observer.drawOccupiedSlot(x, y, t);
		}
	}

	public void drawUnoccupiedSlot(int x, int y) {
		for(int i=0; i<gameObservers.size(); i++)
		{
			GameObserver observer = (GameObserver)gameObservers.get(i);
			observer.drawUnoccupiedSlot(x, y);
		}
	}
	
	public void updateRowAndColValuesOnSlots(int row,int col, int add)
	{
		Slot s;
		for(int i=0;i< getColumns();i++)
		{
			s=(Slot)((ArrayList)theBoard.get(row)).get(i);
			s.setRowSum(s.getRowSum()+add);
			getSlot(row, i).setRowSum(s.getRowSum());
		}

		for(int i=0;i< getRows();i++)
		{
				s=(Slot)((ArrayList)theBoard.get(i)).get(col);
				s.setColSum(s.getColSum()+add);
				getSlot(i, col).setColSum(s.getColSum());
		}

	}
	
	public void updateRowAndColFree(int row, int col){
		Slot s ;
		for(int i=0;i< getColumns();i++)
		{
			s=(Slot)((ArrayList)theBoard.get(row)).get(i);
			s.setRowFree(s.getRowFree()-1);
			getSlot(row, i).setRowFree(s.getRowFree());
			
		}

		for(int i=0;i< getRows();i++)
		{
			s=(Slot)((ArrayList)theBoard.get(i)).get(col);
			s.setColFree(s.getColFree()-1);
			getSlot(i, col).setColFree(s.getColFree());
		}
	}


	public ArrayList<Strategy> getAvalibleStrategies()
	{
		return availibleStrats;
	}

	public boolean stackOfTilesIsEmpty(){
		return stackOfTiles.isEmpty();
	}
	
	public int getRandInt(int value){
		return rand.nextInt(value);
	}
	
	public void randomOrder()
	{
		ArrayList<Player> temp = new ArrayList<Player>();
		while(!turnOrder.isEmpty())
		{
			temp.add(turnOrder.remove(getRandInt(turnOrder.size())));
		}
		turnOrder=temp;
		currentPlayer = turnOrder.get(0);
	}
	
	public void updateScores()					//i know ftechnically only need to update specific row and column, but HOW??? dont know how mcuh of old score was affected by the change
	{
		int rows=0;
		int cols=0;
		for(int i =0; i< turnOrder.size();i++)
		{
			rows=0;
			cols=0;
			for(int r=0;r<getRows();r++)
			{
				rows=rows+scoreRow(turnOrder.get(i),r);
			}
			for(int c=0;c<getColumns();c++)
			{
				cols=cols+scoreCol(turnOrder.get(i),c);
			}
			turnOrder.get(i).setScore(rows+cols);
		}
		
	}
	
	private int scoreRow(Player p,int row)
	{
		int castleScore=0;
		int landScore=0;
		Tile t;
		for(int c=0;c<getColumns();c++)
		{
			t=getSlot(row, c).getTile();
			if(t.getType()=='c' && t.getColor()==p.getColor())
			{
				castleScore=castleScore+t.getValue();
			}
			if(t.getType()=='l')
			{
				landScore=landScore+t.getValue();
			}
		}
		return (castleScore*landScore);
	}
	
	private int scoreCol(Player p,int col)
	{
		int castleScore=0;
		int landScore=0;
		Tile t;
		for(int r=0;r<getRows();r++)
		{
			t=getSlot(r, col).getTile();
			if(t.getType()=='c' && t.getColor()==p.getColor())
			{
				castleScore=castleScore+t.getValue();
			}
			if(t.getType()=='l')
			{
				landScore=landScore+t.getValue();
			}
		}
		return (castleScore*landScore);
		
	}
	
	//the below were added solely to make my j-unit testing easier
	//they only get called from j-unit stuff, so they can be deleted with no harmful effects
	//they are probably poorly coded and unsafe/ignor the whole public private divide that should be had
	//probably would casue bad exception or ugliness if my shiz didnt actually work
	//probalby jsut acessors to some low level stuff to check on it
	//its late
	//update all the things that retrun someithng of enum ReturnStuff...yeah thats all sjut for testing, it wored when they were just voids
	
	public Player getPlayerWithName(String s)
	{
		Player p = null;
		for(int i=0;i<getTurnOrder().size();i++)
		{
			if(((Player)getTurnOrder().get(i)).getName()==s)
			{
				p=((Player)getTurnOrder().get(i));
			}
		}
		return p;
	}
	
	public List getTheBoard()
	{
		return theBoard;
	}
	
	public int numOfEmpty()
	{
		int sum=0;
		for(int r=0;r<getRows();r++)
		{
			for(int c=0;c<getColumns();c++)
			{
				if (getSlot(r,c).getTile().getType()=='e')
				{
					sum=sum+1;
				}
			}
		}
		return sum;
	}

	
}
