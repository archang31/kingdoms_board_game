package view;

import java.awt.Color;

import model.Board;
import model.Player;

import controller.ControllerInterface;
import controller.ReturnStuff;

import model.Tile;

public class TextView implements GameObserver {

	Board model;
	ControllerInterface controller;
	int tileWidth;
	boolean verbose;
	
	public TextView(Board model) {		
		this.model = model;
		model.registerObserver((GameObserver) this);
		verbose=false;
	}
	
	public void createDisplay(int slotWidth) {
		//		 nothing needed
	}
	
	public void drawBackground(){
		//		 nothing needed
	}
	
	public void drawPlayerHand(Player p){
		
	}
	
	public void clear(){
		
	}
	
	public void setVerbose(boolean b)
	{
		verbose=b;
	}

	public ReturnStuff display() {
		// nothing needed
		return ReturnStuff.good;

	}
	
	public void showThisMessage(String s)
	{
		//if(verbose)
		//{
			System.out.println(s);
		//}
	}

	public void drawColumnLabels(int numColumns) {
		// TODO Auto-generated method stub	
		
		if(verbose)
		{
			
		
		System.out.println("Columns: " + numColumns);
		}
	}

	public void drawOccupiedSlot(int x, int y, Tile t) {
		// TODO Auto-generated method stub
		if(verbose)
		{
		System.out.println("Occupied by " + t.getColor().toString() +
				": " + x + "," + y + "   value:" +t.getValue());	
		}
	}

	public void drawRowLabels(int numRows) {
		if(verbose)
		{
		System.out.println("Rows: " + numRows);
		}
	}

	public void drawUnoccupiedSlot(int x, int y) {
		// TODO Auto-generated method stub
		if(verbose)
		{
		System.out.println("Unoccupied: " + x + "," + y);
		}
	}

	public void endRow(int x) {
	}

	public void registerController(ControllerInterface controller) {
		// TODO Auto-generated method stub
		this.controller = controller;
	}

	/*public void myDynamicMenus() {
		// TODO Auto-generated method stub
		
	}*/

}
