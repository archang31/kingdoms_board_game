package view;

import java.awt.Color;

import model.Tile;
import model.Player;

import controller.ControllerInterface;
import controller.ReturnStuff;

public interface GameObserver {

	void registerController(ControllerInterface controller);
	void createDisplay(int SlotWidth);
	void drawUnoccupiedSlot(int x, int y);
	void drawOccupiedSlot(int x, int y, Tile t);// Color tokenColor, int value);
	void drawColumnLabels(int numColumns);
	void drawBackground();
	void drawRowLabels(int numRows);
	ReturnStuff display();
	void showThisMessage(String string);
	void setVerbose(boolean b);
	void drawPlayerHand(Player p);
	void clear();
	//void myDynamicMenus();
	
}
