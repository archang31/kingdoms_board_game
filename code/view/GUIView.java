package view;





import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;

import model.Board;
import model.Player;
import model.Slot;
import model.Tile;

import model.Strategy;

import controller.ControllerInterface;
import controller.GameController;
import controller.ReturnStuff;

public class GUIView implements ActionListener, GameObserver {

	//BoardManagerInterface model;
	Board model;
	ControllerInterface controller;
	int slotWidth;

	JFrame frame;

	JButton showBoardButton;
	JButton setupNewGame;
	JPanel buttonPanel;
	JPanel bp;
	JPanel goGoGo;
	JPanel showB;
	
	JButton rset;
	JButton bset;
	JButton gset;
	JButton yset;
	JButton start;

	JTextField textField;
	JLayeredPane gamePanel;
	
	JMenuBar menuBar;
	
	JMenu gameMenu;
	JMenuItem newGame;
	JMenuItem doTurn;
	JMenuItem doRound;
	JMenuItem doGame;
	
	JMenu viewMenu;
	JMenu viewHand;

	JMenuItem viewOrder;
	JMenuItem viewScore;

	JMenu actionsMenu;
	JMenuItem draw;
	JMenuItem checkMove;
	JMenuItem makeMove;
	
	JMenu strategyMenu;
	JMenu viewStrategy;
	JMenu changeStrategy;	
	
	boolean vervose = false;
	
	JMenuItem exitMenu;
	
	JLabel label;
	JLabel castle;
	JLabel land;
	JLabel player;
	
	public GUIView(Board model) {		
		this.model = model;
		model.registerObserver((GameObserver) this);	
		
	}
	
	public void registerController(ControllerInterface controller) {
		this.controller = controller;
	}
	
	public void createDisplay(int slotWidth){
		this.slotWidth = slotWidth;
		createGUI();
	}
	
	final int COL_OFFSET = 22;
	final int LABEL_OFFSET = 15;
	final int ROW_OFFSET = 85;
	final int BETWEEN_slotS = 20;
	final int LABEL_HEIGHT = 60;
	final int PLAYER_INDENT = 200;
	final int FONT_SIZE = 28;

	public void centerFrame(JFrame f){
	    // Need the toolkit to get info on system.
	    Toolkit tk = Toolkit.getDefaultToolkit ();
	
	    // Get the screen dimensions.
	    Dimension screen = tk.getScreenSize ();
	
	    // Make the frame 1/4th size of screen.
	    int fw =  (int) (screen.getWidth());
	    int fh =  (int) (screen.getHeight());
	    f.setSize (fw-(fw/50),fh-(fh/20));
	
	    // And place it in center of screen.
	    int lx =  (int) (screen.getWidth ()  * 1/32);
	    int ly =  (int) (screen.getHeight () * 1/32);
	    f.setLocation (1,1);
	  } // centerFrame

	public void drawColumnLabels(int numColumns)
	{
		for (int i=1; i<=numColumns; i++){
			JLabel labels = new JLabel(""+i);
			labels.setBounds(
					COL_OFFSET+((i-1)*(slotWidth+BETWEEN_slotS))
					+(int)(slotWidth/2), 
					ROW_OFFSET,
					slotWidth+BETWEEN_slotS, LABEL_HEIGHT);
			gamePanel.add(labels, new Integer(-1), 0);
		}
	}

	public void drawOccupiedSlot(int x, int y, Tile t)          // Color tokenColor, int value)
	{
		//SlotDisplay slot = new SlotDisplay(slotWidth, Color.ORANGE, t.getColor(), t.getValue());
		//showThisMessage(t.getFileName());
		//JOptionPane.showMessageDialog(null,(t.getFileName());
		SlotDisplay slot = new SlotDisplay(slotWidth, t.getFileName());
		slot.setBounds(
				COL_OFFSET + LABEL_OFFSET + (slotWidth + BETWEEN_slotS)*(x-1), 
				ROW_OFFSET + LABEL_OFFSET + (slotWidth + BETWEEN_slotS)*(y-1), 
				slotWidth, slotWidth);
		gamePanel.add(slot, new Integer(-1), 0);
	}

	public void drawRowLabels(int numRows)
	{
		for (int i=1; i<=numRows; i++){	
			JLabel labels = new JLabel("" + i);
			labels.setBounds(
				COL_OFFSET, 
				ROW_OFFSET+LABEL_OFFSET+((i-1)*(slotWidth+BETWEEN_slotS))+(int)(slotWidth/2), 
				slotWidth+BETWEEN_slotS, LABEL_HEIGHT);
			gamePanel.add(labels, new Integer(-1), 0);
		}
	}

	public void drawUnoccupiedSlot(int x, int y)
	{
		/**
		SlotDisplay slot = new SlotDisplay(slotWidth, Color.GREEN, Color.GRAY, 0);
		slot.setBounds(	COL_OFFSET + LABEL_OFFSET + (slotWidth + BETWEEN_slotS)*(x-1), 
						ROW_OFFSET + LABEL_OFFSET + (slotWidth + BETWEEN_slotS)*(y-1), 
						slotWidth, slotWidth);
		gamePanel.add(slot, new Integer(-1), 0);
		*/
	}
	public void drawBackground(){
		Background bg = new Background(10);
		bg.setBounds(10, 10, 1000, 1000);
		int tileSize = 160;
		gamePanel.add(bg, new Integer(-1), 0);
		centerFrame(frame);
	}
	
	public void clear(){
		gamePanel.removeAll();
	}
	
	public void drawPlayerHand(Player p){
		JLabel label = new JLabel("Name:");
        label.setForeground(Color.BLACK);
        Font f = new Font("Next Player", 1, FONT_SIZE);
        label.setFont(f);
		label.setBounds(COL_OFFSET + LABEL_OFFSET + ((slotWidth + BETWEEN_slotS) * model.getColumns()), 
				ROW_OFFSET/2 + LABEL_OFFSET, PLAYER_INDENT, slotWidth);
		gamePanel.add(label, new Integer(-1), 0);
		JLabel playerName = new JLabel(p.getName());
		playerName.setForeground(p.getColor());
        Font pf = new Font(p.getName(), 1, FONT_SIZE);
        playerName.setFont(pf);
        playerName.setBounds(COL_OFFSET + LABEL_OFFSET + ((slotWidth + BETWEEN_slotS) * model.getColumns() + PLAYER_INDENT), 
				ROW_OFFSET/2 + LABEL_OFFSET, PLAYER_INDENT, slotWidth);
		gamePanel.add(playerName, new Integer(-1), 0);
		JLabel land = new JLabel("Land Tiles:");
        land.setFont(f);
		land.setBounds(COL_OFFSET + LABEL_OFFSET + ((slotWidth + BETWEEN_slotS) * model.getColumns()), 
				ROW_OFFSET/2 + LABEL_OFFSET + LABEL_HEIGHT, PLAYER_INDENT, slotWidth);
		gamePanel.add(land, new Integer(-1), 0);
		if (p.landArraySize() == 0){
			//JOptionPane.showMessageDialog(null,"Land Array Size is 0");
		}
		else
		{
			for (int i=0; i<p.landArraySize(); i++){
				SlotDisplay slot = new SlotDisplay(slotWidth, p.onlySeeLand(i).getFileName());
				slot.setBounds(COL_OFFSET + LABEL_OFFSET + ((slotWidth + BETWEEN_slotS) * model.getColumns()+i) + PLAYER_INDENT, 
						ROW_OFFSET/2 + LABEL_OFFSET + LABEL_HEIGHT*2, PLAYER_INDENT, slotWidth);
				gamePanel.add(slot, new Integer(-1), 0);
				
			}
		}
		JLabel castle = new JLabel("Castle Tiles:");
		castle.setFont(f);
		castle.setBounds(COL_OFFSET + LABEL_OFFSET + ((slotWidth + BETWEEN_slotS) * model.getColumns()), 
				ROW_OFFSET/2 + LABEL_OFFSET + LABEL_HEIGHT + slotWidth + BETWEEN_slotS, PLAYER_INDENT, slotWidth);
		gamePanel.add(castle, new Integer(-1), 0);
		
		if (p.castleArraySize() == 0){
		//	JOptionPane.showMessageDialog(null,"Castle Array Size is 0");
		}
		else
		{
			int j = 0;
			int k = 1;
			for (int i=0; i< p.castleArraySize(); i++){
				SlotDisplay slot = new SlotDisplay(slotWidth, p.onlySeeCastle(i).getFileName());
				slot.setBounds(COL_OFFSET + LABEL_OFFSET + ((slotWidth + BETWEEN_slotS) * (model.getColumns()+j)) + PLAYER_INDENT, 
						ROW_OFFSET/2 + LABEL_OFFSET + LABEL_HEIGHT*2 + (slotWidth + BETWEEN_slotS)*k, PLAYER_INDENT, slotWidth);
				gamePanel.add(slot, new Integer(-1), 0);
				j++;
				if (j == 2){
					j = 0;
					k++;
				}
				
			}
		}
	}
	
	public ReturnStuff display()
	{
		gamePanel.setVisible(true);
		return ReturnStuff.good;
	}
	
	public void setVerbose(boolean b)
	{
		vervose=b;
	}

	public void makeThoseFiggityMenues()
	{
	
		ArrayList<Player> people = model.getTurnOrder();
		ArrayList<Strategy> s = model.getAvalibleStrategies();
		JMenuItem temp;
		JMenu menuu;
		
		
	
		for(int i=0;i<people.size();i++)
		{
			
			final Player who=people.get(i);    //palyer menu being added for
			System.out.println(who.getName());
			temp = new JMenuItem(who.getName());                 
			temp.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event) {
					   controller.viewHand(who);}});
			viewHand.add(temp);                                  //view hand subMenus
			
			temp = new JMenuItem(who.getName());
			temp.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event) {
					   controller.showStrategy(who);}});
			viewStrategy.add(temp);                               //view Strategy submenus
			
			
			menuu = new JMenu(who.getName());
			changeStrategy.add(menuu);
			for(int j=0; j < s.size();j++)
			{
				
			final Strategy strat = s.get(j);    //strategy being added
			temp= new JMenuItem("set to " + strat.getName());
			temp.addActionListener(new ActionListener()              
			{
				public void actionPerformed(ActionEvent event)
				{
					controller.setPlayerStrategy(who,strat); 
				}
			});
			
			menuu.add(temp);                          //change stratry submenus
			}
		}
	}
	
	public void myDynamicMenus()
	{
		makeThoseFiggityMenues();
	}

	public void showThisMessage(String s) //my new solution to MVC, so now view tells controler when a buton is clicked, controller tells model to do it then contorl calls this to show to the screen a message
	{
		if(vervose)
		{
			JOptionPane.showMessageDialog(null,s);
		}
		else
		{
			textField.setText(s);
		}
		
	}
	
	
	/*****************************************************************
	 * Event handlers
	 *****************************************************************/
	public void actionPerformed(ActionEvent event){
		if (event.getSource() == showBoardButton){
			controller.showBoard();
			textField.setText("Board displayed.");
			showBoardButton.setEnabled(false);
		}
	
		else if (event.getSource() == exitMenu){
			frame.setVisible(false);
			frame.dispose();
			System.exit(0);
		}
		
	}
	
	/*****************************************************************
	 * GUI Components
	 *****************************************************************/
	
	public void playModeActivated()
	{
		menuBar.remove(exitMenu);   //re add it at the end
		
		bp.setVisible(false);
		goGoGo.setVisible(false);
		
		
		
		newGame.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				   controller.newGame();}});
		doTurn = new JMenuItem("Do Turn");
		doTurn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				   controller.doTurn();}});
		doRound = new JMenuItem("Do Round");		
		doRound.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				   controller.doRound();}});
		doGame = new JMenuItem("Do Game");		
		doGame.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				   controller.doGame();}});
		
		/**
		 View Menu
		 */
		
		viewMenu = new JMenu("View");
		viewHand = new JMenu("View Hand");  //dynamically created based on players playing
		

		
		
		viewOrder = new JMenuItem("View Order");		
		viewOrder.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				   controller.showOrder();}});
		viewScore = new JMenuItem("View Score");
		viewScore.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				   controller.displayScore();}});
	
		
		viewMenu.add(viewHand);
		viewMenu.add(viewOrder);
		viewMenu.add(viewScore);
		
		menuBar.add(viewMenu);
		
		/**
		 Actions
		 */
		
		actionsMenu = new JMenu("Actions");
		draw = new JMenuItem("Draw");		
		draw.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				   controller.draw();}});
		checkMove = new JMenuItem("Check Move");
		checkMove.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
					Slot s = new Slot(0,0);
					s.assignTile(new Tile('e',0,Color.PINK, null));                   ///BADDDDD in future have it so the user can actually create a move to check *****
				  controller.checkMove(s);}});
		makeMove = new JMenuItem("Make Move");
		makeMove.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				   controller.doMove();}});
		actionsMenu.add(draw);
		actionsMenu.add(checkMove);
		actionsMenu.add(makeMove);
		
		menuBar.add(actionsMenu);
		
		/**
		 Strategy
		 */
		
		
		
		strategyMenu = new JMenu("Strategy");
		viewStrategy = new JMenu("View Strategy");
		changeStrategy = new JMenu("Change Strategy");
		
		strategyMenu.add(viewStrategy);
		strategyMenu.add(changeStrategy);
		menuBar.add(strategyMenu);

		gameMenu.add(doTurn);
		gameMenu.add(doRound);
		gameMenu.add(doGame);
		
		menuBar.add(exitMenu);
		
		
		showB.setVisible(true);
		gamePanel.setVisible(false);
	}
	
	
	
	public void createGUI() {
		
		menuBar = new JMenuBar();
		frame = new JFrame();
		frame.setLayout(new BorderLayout());

		final int FIELD_WIDTH = 20;
		textField = new JTextField(FIELD_WIDTH);
		textField.setText("Click a button or menu choice!");

		setupNewGame = new JButton("setup a New Game");
		setupNewGame.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				   ((GameController) controller).lineItUp();}});


		buttonPanel = new JPanel();
		buttonPanel.add(setupNewGame);

		gamePanel = new JLayeredPane();

		menuBar = new JMenuBar();

		/**
		 Game Menu
		 */
		gameMenu = new JMenu("Game Menu");
		newGame = new JMenuItem("New Game");
		
		
		gameMenu.add(newGame);

		menuBar.add(gameMenu);
	
		
		/**
		 * Exit menuItem
		 */
		exitMenu = new JMenuItem("Exit");
		exitMenu.addActionListener(this);
		
		menuBar.add(exitMenu);

		frame.setJMenuBar(menuBar);

		frame.getContentPane().add(gamePanel, BorderLayout.CENTER);
		frame.getContentPane().add(buttonPanel, BorderLayout.NORTH);
		frame.getContentPane().add(textField, BorderLayout.SOUTH);
		
		rset = new JButton("setup Red Player");
		rset.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				setupPlayerPopUp(((GameController) controller).getRed());}});
		
		bset = new JButton("setup Blue Player");
		bset.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				setupPlayerPopUp(((GameController) controller).getBlue());}});
		
		gset = new JButton("setup Green Player");
		gset.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				setupPlayerPopUp(((GameController) controller).getGreen());}});
		yset = new JButton("setup Yellow Player");
		yset.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				setupPlayerPopUp(((GameController) controller).getYellow());}});
		
		bp = new JPanel();
		bp.add(rset);
		bp.add(bset);
		bp.add(gset);
		bp.add(yset);
		
		
		frame.getContentPane().add(bp, BorderLayout.EAST);
		bp.setVisible(false);
		
		start = new JButton("Start The Game");
		start.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event) {
				((GameController) controller).next();}});
		
		goGoGo = new JPanel();
		goGoGo.add(start);
		
		frame.getContentPane().add(goGoGo, BorderLayout.WEST);
		
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setSize(500, 500);
		frame.setVisible(true);
		gamePanel.setVisible(false);
		goGoGo.setVisible(false);
		
		textField.setVisible(true);
		
		showBoardButton = new JButton("Show Board");
		showBoardButton.addActionListener(this);


		showB = new JPanel();
		showB.add(showBoardButton);
		frame.getContentPane().add(showB, BorderLayout.NORTH);
		showB.setVisible(false);
		
		centerFrame(frame);
	}
	
	public void preGameMenus()
	{
		buttonPanel.setVisible(true);
		menuBar = new JMenuBar();

		/**
		 Game Menu
		 */
		gameMenu = new JMenu("Game Menu");
		newGame = new JMenuItem("New Game");
		gameMenu.add(newGame);
		menuBar.add(gameMenu);
		/**
		 * Exit menuItem
		 */
		exitMenu = new JMenuItem("Exit");
		exitMenu.addActionListener(this);
		menuBar.add(exitMenu);

		frame.setJMenuBar(menuBar);

		frame.pack();
		frame.setSize(500, 500);
		frame.setVisible(true);
		gamePanel.setVisible(false);
		goGoGo.setVisible(false);
		
		textField.setVisible(true);
		
		showBoardButton.setEnabled(true);
		showB.setVisible(false);
		
		centerFrame(frame);

	}

	public static ActionListener createButtonListener(final String message) {
		return new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(null, message);
			}
		};
	}
	
	public void setupPlayersMenus()
	{
		buttonPanel.setVisible(false);
		bp.setVisible(true);
		goGoGo.setVisible(true);
		
	}
	
	public void setupPlayerPopUp(final Player p)
	{
		final JFrame reg = new JFrame("Setup "+p.getName());
		
		JButton done = new JButton("Done");
		
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();

		JPanel mainPanel = new JPanel();
			
		JLabel l1 = new JLabel("Player name:");
		final JTextField name = new JTextField(30);
		name.setText(p.getName());
		panel1.add(l1);
		panel1.add(name);
		
		JLabel l2 = new JLabel("Select Strategy:");
		final JComboBox criteria = new JComboBox();
		for(int i=0;i<((GameController) controller).getModel().getAvalibleStrategies().size();i++)
		{
			criteria.addItem(((GameController) controller).getModel().getAvalibleStrategies().get(i).getName());
		}
		criteria.setSelectedIndex(((GameController) controller).getModel().getAvalibleStrategies().indexOf(p.getStrat()));
		panel2.add(l2);
		panel2.add(criteria);
			
		
		
		done.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event)
			{
				p.setName(name.getText());
			//	if (criteria.getSelectedIndex()>0)
			//	{
				p.setStrat(((GameController) controller).getModel().getAvalibleStrategies().get(criteria.getSelectedIndex()));
			//	}
				reg.setVisible(false);
			}
		});
			
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		mainPanel.add(panel1);
		mainPanel.add(panel2);

		
		
		reg.add(mainPanel, BorderLayout.CENTER);
		reg.add(done, BorderLayout.SOUTH);
		
		reg.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		reg.pack();
		reg.setVisible(true);
		
		
	}
	
	public void userInput(final Player p,final Slot s)
	{

		final JFrame us = new JFrame(p.getName()+"'s Move");
		
		JButton done = new JButton("Done");
		JPanel mainPanel = new JPanel();

		JPanel panel2 = new JPanel();
		JPanel panel1 = new JPanel();
		
			

		
		JLabel l2 = new JLabel("Select Tile:");
		final JComboBox criteria2 = new JComboBox();
		
		for(int i=0; i< p.getCastleArray().size();i++)
		{
			criteria2.addItem(""+((Tile) p.getCastleArray().get(i)).getType()+ " "+((Tile) p.getCastleArray().get(i)).getValue());
		}
		for(int j=0; j< p.getLandArray().size();j++)
		{
			criteria2.addItem(""+((Tile) p.getLandArray().get(j)).getType()+ " "+((Tile) p.getLandArray().get(j)).getValue());
		}
		
		JLabel l1 = new JLabel("Select Coords:");
		final JComboBox criteria1 = new JComboBox();
		
		for(int r=0;r<model.getRows();r++)
		{
			for(int c=0; c<model.getColumns();c++)
			{
				if(model.getSlot(r, c).isEmpty())
				{
					criteria1.addItem("("+r+","+c+")");
				}
			}
		}
		
		
		
		criteria2.setSelectedIndex(0);
		panel1.add(l1);
		panel1.add(criteria1);
		panel2.add(l2);
		panel2.add(criteria2);
			
		

		done.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event)
			{
				if(criteria2.getSelectedIndex()<p.castleArraySize())
				{
					s.assignTile((Tile) p.getCastleArray().remove(criteria2.getSelectedIndex()));
				}
				else
				{
					s.assignTile((Tile) p.getLandArray().remove(criteria2.getSelectedIndex()-p.getCastleArray().size()));
				}
				
				s.setX(Integer.parseInt(((String)criteria1.getSelectedItem()).substring(1, 1)));
				s.setY(Integer.parseInt(((String)criteria1.getSelectedItem()).substring(3, 3)));
				us.setVisible(false);
			}
		});
			
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		mainPanel.add(panel1);
		mainPanel.add(panel2);

		
		
		us.add(mainPanel, BorderLayout.CENTER);
		us.add(done, BorderLayout.SOUTH);
		
		us.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		us.pack();
		us.setVisible(true);
	}

}

