package view;

import java.awt.Color;
import java.awt.Image;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Graphics;
import javax.swing.JComponent;

public class Background extends JComponent{

	private static final long serialVersionUID = 1L;
	int tileWidth;
	int tokenSize;

	public Background(int tileWidth) {
		super();
		this.tileWidth = tileWidth;
		this.tokenSize = (int)(tileWidth/4);
	}

	public void paintComponent(Graphics g)
	{
		Image img = Toolkit.getDefaultToolkit().getImage("pics/board.jpg");
		g.drawImage(img, tokenSize, tokenSize, this);
	}

}
