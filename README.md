# Kingdoms Board Game Adaptation

[![unlicense](https://img.shields.io/badge/un-license-brightgreen.svg)](http://unlicense.org "The Unlicense") [![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme "RichardLitt/standard-readme")

> This is a java object-oriented ([model-view-controller UI](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller)) implementation of the Kingdoms Board Game with computer-played strategies from 2007.

This was a project done with [Roy Ragsdale](https://gitlab.com/royragsdale) for West Point's CS403 (Object-Oriented Concepts) in the Spring of 2007. The final project was to implement a game, and we picked [Reiner Knizia's Kingdoms](https://www.fantasyflightgames.com/en/news/2011/12/1/a-guide-to-kingdom-building/). The game was created solely for this educational purpose. The game concept and all game images are intellectual property of Fantasy Flight Publishing.
See [specs_and_documentation.pdf](./specs_and_documentation.pdf) for more details.

## Table of Contents

- [Methodology](#background)
- [Usage](#usage)
- [Game Rules](#game-rules)
- [Contribute](#contribute)
- [License](#license)

## Methodology & Code Breakdown

See [specs_and_documentation.pdf](./specs_and_documentation.pdf) for more details on design decisions and code breakdown. See [code_snippets_presentation.pdf](./code_snippets_presentation.pdf) for code snippets pertaining to strategy, state, and patterns.

### Unified Model Language (UML) Diagram

<img src="./images/UML.jpeg" width="100%">

### Player Strategies

We created four strategies that you can play against each other, or you can select **Human Player** to manually play each turn. Below, we break down the four strategies.

##### BENEFITS-SELF

If a player P has a spot (x, y) where the total of all resources in the given row and column is at least positive 12 and there are only three empty board spaces in that row/column, the player will play a castle of rank 1. The player will also play a castle of rank 1 if the total is at least 7 and there are only two board spaces, or the total is at least 1 and there is only 1 space. For every 3 points above the minimum total amount, the player will increase the rank of the castle by 1 (for example, if there is only one free space in row/column combination and the total is 1, he will play a rank one castle. If the total is 4, he will play a rank 2 castle, 7 a rank 3 and 10 or more a rank 4). The player will prioritize the row/column combinations based on total resource value, subtracting 11 from the combination with 3 empty slots and 6 from the combination with 2 empty slots. If none of these conditions are satisfied, the player will draw and play a land tile, prioritizing the two land tiles by the highest resource number. If there are no land tiles left, the player will just play his current land tile.

##### HURTS-OTHERS

This strategy’s goal is to cause the maximum negative effect on the apposing players. This strategy first searches the board to determine the location that affects the most enemy castles by value while affecting the minimum rank of castles possible of the current players color. If the slot affects at least 4 ranks of enemies and the player has a negative tile, they will play the tile to hurt the enemy. If this player has a 1 or 2 land, he will play the land on the spot that most positively affects his own castles. Otherwise, he will play the lowest castle he has.

##### BIGGEST

This strategy will rely on the fact that there are a greater number of positive resource tiles than negative ones and that the game generally moves in an upward trend. As such, this player will play the biggest castle tiles whenever possible. In a four player game, each player will get a max of 8 turns (30 slots/ 4 players = 7.5). As such, this player will attend to use attempt to use all of their castle tiles except the rank 1 castles. This player will begin the game by placing a rank 2 castle tile on the board to bait initial attacks unless they hold a resource tile greater than 3, in which case they will draw and use the highest level resource tile. For their second turn, this player will place a rank 3 castle tile on the board unless he has a resource tile great than 3, in which case they will draw and use the highest level resource tile. They will then continue to place castle tiles, highest rank first, until all that remains are rank 2 and below castles, only drawing and playing a resource tiles if the value is greater than 2. Once only rank two castles remain, this player will draw and place resource tiles unless a spot is available that has a resource value of 10 plus 3 for every additional empty space in that row/column combination. This player will not play a hazard tile until all that remains are rank one tiles, and only then if the result will not lower his own resource total.

##### DUMB STRATEGY

This strategy simply makes a valid move. It has a 50/50 chance to lay either a castle or land (assuming has both). If it picks a land, it will draw a land first. Once it decides castle or land, it randomly chooses among the values of tiles the player has. This strategy then randomly assigns this tile to a valid empty slot.

## Usage

Download the repo. Make sure you have java and javac install like in this [doc](https://introcs.cs.princeton.edu/java/15inout/mac-cmd.html)

```
archang31$ java -version
java version "1.8.0_191"
Java(TM) SE Runtime Environment (build 1.8.0_191-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.191-b12, mixed mode)

archang31$ javac -version
javac 1.8.0_191
```

Compile and run [GameTester.java](./code/GameTester.java) with `javac`. NOTE - you might get an error and need to use `javac GameTester.java -Xlint:unchecked`.

Then run with `java`.

```
archang31$ javac GameTester.java -Xlint:unchecked
archang31$ java GameTester
```

## Game Rules

<img src="./images/instructions.jpg" width="100%">

[Official Board Game Instructions](./kingdoms_rules.pdf)

## Contribute

> Contributors to this project are expected to adhere to our [Code of Conduct](docs/CODE_OF_CONDUCT.md "Code of Conduct").

I welcome [issues](docs/issue_template.md "Issue template"), but I prefer [pull requests](docs/pull_request_template.md "Pull request template")! See the [contribution guidelines](contributing.md "Contributing") for more information.

## License

This code is [set free](LICENSE).
